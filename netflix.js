var enabled = false;

browser.browserAction.onClicked.addListener(function(tab) {
  if (enabled === false) {
    if (document.getElementById('netflix-player') !== null) {
      browser.tabs.executeScript({
        code:
          'document.querySelector("#netflix-player > div.player-video-wrapper > div > video").style.cssText = "position: relative; width: 134%; height: 134%; margin-left:-430px; margin-top:-180px;"'
      });
    } else {
      browser.tabs.executeScript({
        code:
          'document.querySelector(".VideoContainer > div > video").style.cssText = "position: relative; width: 134%; height: 134%; margin-left:-430px; margin-top:-180px;"'
      });
    }

    browser.browserAction.setIcon({
      path: 'img/icon_enabled.png',
      tabId: tab.id
    });
    enabled = true;
  } else {
    if (document.getElementById('netflix-player') !== null) {
      browser.tabs.executeScript({
        code:
          'document.querySelector("#netflix-player > div.player-video-wrapper > div > video").style.cssText = "position: relative; width: 100%; height: 100%;"'
      });
    } else {
      browser.tabs.executeScript({
        code:
          'document.querySelector(".VideoContainer > div > video").style.cssText = "position: relative; width: 100%; height: 100%;"'
      });
    }
    browser.browserAction.setIcon({ path: 'img/icon.png', tabId: tab.id });
    enabled = false;
  }
});
